package com.example.td3_android;

class JeuVideo {

    private String name;
    private int price;

    public JeuVideo(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return this.name;
    }


    public int getPrice() {
        return this.price;
    }
}
