package com.example.td3_android;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //CREER ICI UNE LISTE DE JEUX VIDEO NOMMEE mesJeux ET REMPLISSEZ LA DE JeuxVideo
        ArrayList<JeuVideo> mesJeux = new ArrayList<JeuVideo>();
        mesJeux.add(new JeuVideo("Zelda",40));
        mesJeux.add(new JeuVideo("Sonic", 10));
        mesJeux.add(new JeuVideo("Bomberman", 1));
        mesJeux.add(new JeuVideo("Luigi's Mansion", 45));
        mesJeux.add(new JeuVideo("MarioKart",35));


        RecyclerView myRecyclerView = findViewById(R.id.myRecyclerView);
        //myRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL, false));
        //myRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL, false));
        myRecyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3));
        myRecyclerView.setAdapter( new MyVideosGamesAdapter(mesJeux));
    }
}
