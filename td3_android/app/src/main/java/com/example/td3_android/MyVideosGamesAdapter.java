package com.example.td3_android;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;


public class MyVideosGamesAdapter extends RecyclerView.Adapter<MyVideosGamesViewHolder> {
    private List<JeuVideo> mesJeux;

    public MyVideosGamesAdapter(List<JeuVideo> mesJeux) {

        this.mesJeux = mesJeux;
    }

    @NonNull
    @Override
    public MyVideosGamesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_jeu_video,
                parent, false);
        return new MyVideosGamesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyVideosGamesViewHolder holder, int position) {
        holder.display(mesJeux.get(position));
    }

    @Override
    public int getItemCount() {

        return mesJeux.size();
    }
}

