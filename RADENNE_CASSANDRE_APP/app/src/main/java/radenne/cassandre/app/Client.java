package radenne.cassandre.app;

import java.util.List;

import radenne.cassandre.app.dto.EleveDto;
import retrofit2.Call;
import retrofit2.http.GET;

public interface Client {

    @GET("eilco.json")
    Call<List<EleveDto>> getEleves();
}
