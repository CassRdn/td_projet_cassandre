package radenne.cassandre.app;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.List;

import radenne.cassandre.app.dto.EleveDto;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private String url = "http://android.busin.fr/";
    private String email="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button = findViewById(R.id.EleveDetail);

        final List<EleveDto> eleves;

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();
        Client client = retrofit.create(Client.class);
        Call<List<EleveDto>> call = client.getEleves();
        call.request().url().toString();
        call.enqueue(new Callback<List<EleveDto>>() {
            @Override
            public void onResponse(Call<List<EleveDto>> call, Response<List<EleveDto>> response) {
                List<EleveDto> list = response.body();
                RecyclerView myRecyclerView = findViewById(R.id.myRecyclerView);
                LinearLayoutManager layout= new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL, false);
                myRecyclerView.setLayoutManager(layout);
                myRecyclerView.setAdapter( new EleveAdapter(list));

            }

            @Override
            public void onFailure(Call<List<EleveDto>> call, Throwable t) {
                Toast.makeText(MainActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });


    }

}
