package radenne.cassandre.app.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NationalityDto {

    @SerializedName("nationality")
    @Expose
    private String nationality;
    @SerializedName("flag")
    @Expose
    private String flag;
    @SerializedName("nationnality")
    @Expose
    private String nationnality;

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getNationnality() {
        return nationnality;
    }

    public void setNationnality(String nationnality) {
        this.nationnality = nationnality;
    }

}