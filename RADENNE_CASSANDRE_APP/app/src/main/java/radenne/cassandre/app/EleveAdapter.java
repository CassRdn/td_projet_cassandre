package radenne.cassandre.app;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import radenne.cassandre.app.dto.EleveDto;

public class EleveAdapter extends RecyclerView.Adapter<EleveViewHolder> {
    private List<EleveDto> eleves;
    public EleveAdapter(List<EleveDto> eleves) {
        this.eleves = eleves;
    }

    @NonNull
    @Override
    public EleveViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_eleve, parent,false);
        return new EleveViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EleveViewHolder holder, int position) {
        holder.display((eleves.get(position)));
    }

    @Override
    public int getItemCount() {
        return eleves.size();
    }
}
