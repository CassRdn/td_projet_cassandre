package radenne.cassandre.app;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import radenne.cassandre.app.dto.EleveDto;

public class EleveViewHolder extends RecyclerView.ViewHolder {
    private ImageView pic;
    private TextView nomEleve;
    private TextView emailEleve;
    private TextView telephoneEleve;

    public EleveViewHolder(@NonNull View itemView) {
        super(itemView);
        pic = itemView.findViewById(R.id.EleveImage);
        nomEleve = itemView.findViewById(R.id.NomEleve);
        emailEleve = itemView.findViewById(R.id.EmailEleve);
        telephoneEleve = itemView.findViewById(R.id.TelephoneEleve);
    }
    void display(EleveDto eleve) {
        Picasso.get().load(eleve.getPicture()).into(pic);
        nomEleve.setText(eleve.getName().getLast()+" "+eleve.getName().getFirst());
        emailEleve.setText(eleve.getEmail());
        telephoneEleve.setText(eleve.getPhone());

    }}