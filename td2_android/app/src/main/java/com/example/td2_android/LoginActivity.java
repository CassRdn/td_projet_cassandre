package com.example.td2_android;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private final String TAG= "NewsList";
    private String strName = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Button button = findViewById(R.id.btn_login);
        button.setOnClickListener(this);
        EditText login = findViewById(R.id.nomLog);


}

    @Override
    public void onClick(View v) {

        NewsListApplication app = (NewsListApplication)getApplicationContext();
        String login = app.getLogin();
        EditText login2 = findViewById(R.id.nomLog);
        String s = login2.getText().toString();
        Intent intent = new Intent(LoginActivity.this, NewsActivity.class);
        intent.putExtra("nomLog", login);
        startActivity(intent);

    }

    @Override
    public void onBackPressed(){
        onDestroy();
        finish();
    }


    @Override
    protected void onDestroy(){
        super.onDestroy();
        Log.i(TAG,"terminaison de l'activité" + getLocalClassName());
    }


}
