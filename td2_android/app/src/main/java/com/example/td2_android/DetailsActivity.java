package com.example.td2_android;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class DetailsActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Button button_ok=findViewById(R.id.btn_ok);

        button_ok.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, NewsActivity.class);

        startActivity(intent);

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, NewsActivity.class);

        startActivity(intent);
    }
}
