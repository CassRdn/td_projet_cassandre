package com.example.td2_android;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class NewsActivity extends AppCompatActivity implements View.OnClickListener {

    private final String TAG ="NewsList";
    private String url ="http://android.busin.fr/";
    String str = "";
    private TextView Textv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        Intent intent = getIntent();

        Button button_details=findViewById(R.id.btn_details);
        button_details.setOnClickListener(this);

        Button button_logout=findViewById(R.id.btn_logout);
        button_logout.setOnClickListener(this);

        Button button_about=findViewById(R.id.btn_about);
        button_about.setOnClickListener(this);

        Textv = (TextView)findViewById(R.id.textLog);
        Bundle extras = getIntent().getExtras();
            if(extras!=null){
                str = (String)extras.get("nomLog");
                Textv.setText("Bonjour " +str);



        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_details:
                startActivity(new Intent(this,DetailsActivity.class));
                break;

            case R.id.btn_logout:
                startActivity(new Intent(this, LoginActivity.class));
                break;

            case R.id.btn_about:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                break;


        }
    }
    @Override
    public void onBackPressed(){
        onDestroy();
        finish();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG,"terminaison de l'activité"+getLocalClassName());
    }

}
