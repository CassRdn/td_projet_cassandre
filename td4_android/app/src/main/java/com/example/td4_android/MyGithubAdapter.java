package com.example.td4_android;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MyGithubAdapter extends RecyclerView.Adapter<MyGithubViewHolder> {
    private List<RepoList> repoList;
    public MyGithubAdapter(List<RepoList> repoList) {
        this.repoList = repoList;
    }
    @NonNull
    @Override
    public MyGithubViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_github_repo,
                parent, false);
        return new MyGithubViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull MyGithubViewHolder holder, int position) {
        holder.display(repoList.get(position));
    }
    @Override
    public int getItemCount() {
        return repoList.size();
    }
}
