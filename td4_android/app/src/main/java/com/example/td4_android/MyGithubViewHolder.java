package com.example.td4_android;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyGithubViewHolder extends RecyclerView.ViewHolder {
    private TextView mNamerepo;

    public MyGithubViewHolder(@NonNull View itemView) {
        super(itemView);
        mNamerepo = itemView.findViewById(R.id.name);
    }

    void display(RepoList repoList) {
        mNamerepo.setText(repoList.getRepoName());

    }
}