package com.example.hogwartsguide;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hogwartsguide.dto.CharacterDto;

public class CharactersViewHolder extends RecyclerView.ViewHolder {
    private TextView characterText;
    private TextView house;

    public CharactersViewHolder(@NonNull View itemView) {
        super(itemView);
        characterText = itemView.findViewById(R.id.character);
        house = itemView.findViewById(R.id.house);
    }
    void display(CharacterDto character) {
        characterText.setText(character.getName());
        house.setText(character.getHouse());
    }

}
