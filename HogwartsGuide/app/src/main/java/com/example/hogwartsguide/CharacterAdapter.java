package com.example.hogwartsguide;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hogwartsguide.dto.CharacterDto;

import java.util.List;

public class CharacterAdapter extends RecyclerView.Adapter<CharactersViewHolder> {
    private List<CharacterDto>characters;
    public CharacterAdapter(List<CharacterDto>characters) {
        this.characters = characters;
    }

    @NonNull
    @Override
    public CharactersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_character, parent,false);
        return new CharactersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CharactersViewHolder holder, int position) {
    holder.display((characters.get(position)));
    }

    @Override
    public int getItemCount() {
    return characters.size();
    }
}
