package com.example.hogwartsguide;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;


import com.example.hogwartsguide.dto.HouseDto;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HatActivity extends AppCompatActivity implements View.OnClickListener {

    private String url = "https://www.potterapi.com/v1/";
    private String key = "$2a$10$at2TU1IFUGGfbh8aPHwM..lfM2IaOHNp5/NiU6NLpdKFY2tH4KxDi";
    private String gryffKey = "5a05e2b252f721a3cf2ea33f";
    private String slythKey = "5a05dc8cd45bd0a11bd5e071";
    private String huffKey = "5a05dc58d45bd0a11bd5e070";
    private String ravenKey = "5a05da69d45bd0a11bd5e06f";
    private String keyHouse = "";
    private String houseName ="";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hat);

        ImageButton button = findViewById(R.id.hatButton);
        button.setOnClickListener(this);
        ImageButton buttonCharacter = findViewById(R.id.characterButton);
        buttonCharacter.setOnClickListener(this);
        ImageButton buttonSpell = findViewById(R.id.spellBouton);
        buttonSpell.setOnClickListener(this);
        ImageButton buttonApropos = findViewById(R.id.AproposBouton);
        buttonApropos.setOnClickListener(this);
        ImageButton buttonPrefect = findViewById(R.id.prefectBouton);
        buttonPrefect.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.characterButton:
                startActivity(new Intent(v.getContext(), CharacterActivity.class));
                break;

            case R.id.hatButton:
                Retrofit.Builder builder = new Retrofit.Builder()
                        .baseUrl(url)
                        .addConverterFactory(GsonConverterFactory.create());

                Retrofit retrofit = builder.build();
                HogwartsClient client = retrofit.create(HogwartsClient.class);
                Call<String> call = client.SortingHat();
                call.enqueue(new Callback<String>() {

                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        TextView house = findViewById(R.id.houseName);
                        house.setText(response.body());
                        getHeadOfHouse(response.body());
                        houseName = response.body();


                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Toast.makeText(HatActivity.this, "Error...!!!", Toast.LENGTH_SHORT).show();
                    }

                });
                break;

            case R.id.spellBouton:
                startActivity(new Intent(v.getContext(), SpellActivity.class));
                break;

            case R.id.prefectBouton:
                String NameOfHouse = houseName;
                Intent intent =new Intent(HatActivity.this, PrefectActivity.class);
                intent.putExtra("house", NameOfHouse);
                startActivity(intent);
                break;


            case R.id.AproposBouton:
                AlertDialog.Builder builderDial = new AlertDialog.Builder(v.getContext());
                builderDial.setCancelable(true);
                builderDial.setTitle("About HogwartsGuide");
                builderDial.setMessage("Developped by Cassandre RADENNE ING3 using the PotterAPI. If you need any help : cassandre.radenne.elv@eilco-ulco.fr.\n\n  " +
                        "First press the Hat Button : it will tell you which house do you belong to. You'll get the head of your House. \n\n  " +
                        "Prefect button : it will tell you the names of the people in your house.\n\n   " +
                        "Contacts Button : it will tell you about all the characters in the HP universe and their house. \n\n  " +
                        "Spell Button : it will tell you all about the spells.\n\n  ");
                AlertDialog dialog = builderDial.create();
                dialog.show();
                break;
        }
    }

    public void getHeadOfHouse(String house) {

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();
        HogwartsClient client = retrofit.create(HogwartsClient.class);
        if ("Gryffindor".equals(house)) {
            this.keyHouse = gryffKey;
        }
        if ("Ravenclaw".equals(house)) {
            this.keyHouse = ravenKey;
        }
        if ("Slytherin".equals(house)) {
            this.keyHouse = slythKey;
        }
        if ("Hufflepuff".equals(house)) {
            this.keyHouse = huffKey;
        }
        Call<List<HouseDto>> call = client.getHouse(this.keyHouse, key);
        String urlcalled = call.request().url().toString();
        call.enqueue(new Callback<List<HouseDto>>() {
            @Override
            public void onResponse(Call<List<HouseDto>>call, Response<List<HouseDto>> response) {
                TextView headOfHouse = findViewById(R.id.headHouseName);
                headOfHouse.setText(response.body().get(0).getHeadOfHouse());
            }

            @Override
            public void onFailure(Call<List<HouseDto>> call, Throwable t) {
                Toast.makeText(HatActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }

}