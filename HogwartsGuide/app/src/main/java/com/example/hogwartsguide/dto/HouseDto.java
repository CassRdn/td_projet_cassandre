package com.example.hogwartsguide.dto;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.List;
import java.util.Objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HouseDto {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("mascot")
    @Expose
    private String mascot;
    @SerializedName("headOfHouse")
    @Expose
    private String headOfHouse;
    @SerializedName("houseGhost")
    @Expose
    private String houseGhost;
    @SerializedName("founder")
    @Expose
    private String founder;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("school")
    @Expose
    private String school;
    @SerializedName("members")
    @Expose
    private List<MemberDto> members = null;
    @SerializedName("values")
    @Expose
    private List<String> values = null;
    @SerializedName("colors")
    @Expose
    private List<String> colors = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public HouseDto() {
    }

    /**
     *
     * @param headOfHouse
     * @param houseGhost
     * @param mascot
     * @param school
     * @param founder
     * @param v
     * @param members
     * @param values
     * @param name
     * @param id
     * @param colors
     */
    public HouseDto(String id, String name, String mascot, String headOfHouse, String houseGhost, String founder, Integer v, String school, List<MemberDto> members, List<String> values, List<String> colors) {
        super();
        this.id = id;
        this.name = name;
        this.mascot = mascot;
        this.headOfHouse = headOfHouse;
        this.houseGhost = houseGhost;
        this.founder = founder;
        this.v = v;
        this.school = school;
        this.members = members;
        this.values = values;
        this.colors = colors;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMascot() {
        return mascot;
    }

    public void setMascot(String mascot) {
        this.mascot = mascot;
    }

    public String getHeadOfHouse() {
        return headOfHouse;
    }

    public void setHeadOfHouse(String headOfHouse) {
        this.headOfHouse = headOfHouse;
    }

    public String getHouseGhost() {
        return houseGhost;
    }

    public void setHouseGhost(String houseGhost) {
        this.houseGhost = houseGhost;
    }

    public String getFounder() {
        return founder;
    }

    public void setFounder(String founder) {
        this.founder = founder;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public List<MemberDto> getMembers() {
        return members;
    }

    public void setMembers(List<MemberDto> members) {
        this.members = members;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

    public List<String> getColors() {
        return colors;
    }

    public void setColors(List<String> colors) {
        this.colors = colors;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HouseDto houseDto = (HouseDto) o;
        return Objects.equals(id, houseDto.id) &&
                Objects.equals(name, houseDto.name) &&
                Objects.equals(mascot, houseDto.mascot) &&
                Objects.equals(headOfHouse, houseDto.headOfHouse) &&
                Objects.equals(houseGhost, houseDto.houseGhost) &&
                Objects.equals(founder, houseDto.founder) &&
                Objects.equals(v, houseDto.v) &&
                Objects.equals(school, houseDto.school) &&
                Objects.equals(members, houseDto.members) &&
                Objects.equals(values, houseDto.values) &&
                Objects.equals(colors, houseDto.colors);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public int hashCode() {
        return Objects.hash(id, name, mascot, headOfHouse, houseGhost, founder, v, school, members, values, colors);
    }

    @Override
    public String toString() {
        return "HouseDto{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", mascot='" + mascot + '\'' +
                ", headOfHouse='" + headOfHouse + '\'' +
                ", houseGhost='" + houseGhost + '\'' +
                ", founder='" + founder + '\'' +
                ", v=" + v +
                ", school='" + school + '\'' +
                ", members=" + members +
                ", values=" + values +
                ", colors=" + colors +
                '}';
    }
}
