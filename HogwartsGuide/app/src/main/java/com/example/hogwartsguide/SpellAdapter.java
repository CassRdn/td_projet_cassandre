package com.example.hogwartsguide;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hogwartsguide.dto.SpellDto;

import java.util.List;

public class SpellAdapter extends RecyclerView.Adapter<SpellViewHolder> {
    private List<SpellDto> spells;
    public SpellAdapter(List<SpellDto>spells) {
        this.spells = spells;
    }

    @NonNull
    @Override
    public SpellViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_spell, parent,false);
        return new SpellViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SpellViewHolder holder, int position) {
        holder.display((spells.get(position)));
    }

    @Override
    public int getItemCount() {
        return spells.size();
    }
}

