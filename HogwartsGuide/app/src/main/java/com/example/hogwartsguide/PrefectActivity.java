package com.example.hogwartsguide;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hogwartsguide.dto.CharacterDto;
import com.example.hogwartsguide.dto.HouseDto;
import com.example.hogwartsguide.dto.MemberDto;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PrefectActivity extends AppCompatActivity {
    private String url = "https://www.potterapi.com/v1/";
    private String key = "$2a$10$at2TU1IFUGGfbh8aPHwM..lfM2IaOHNp5/NiU6NLpdKFY2tH4KxDi";
    private String gryffKey = "5a05e2b252f721a3cf2ea33f";
    private String slythKey = "5a05dc8cd45bd0a11bd5e071";
    private String huffKey = "5a05dc58d45bd0a11bd5e070";
    private String ravenKey = "5a05da69d45bd0a11bd5e06f";
    private String keyHouse = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        String nameOfHouse;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character);
        Intent intent =getIntent();
        nameOfHouse = intent.getStringExtra("house");
        final List<MemberDto> characters;

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();
        HogwartsClient client = retrofit.create(HogwartsClient.class);
        if ("Gryffindor".equals(nameOfHouse)) {
            this.keyHouse = gryffKey;
        }
        if ("Ravenclaw".equals(nameOfHouse)) {
            this.keyHouse = ravenKey;
        }
        if ("Slytherin".equals(nameOfHouse)) {
            this.keyHouse = slythKey;
        }
        if ("Hufflepuff".equals(nameOfHouse)) {
            this.keyHouse = huffKey;
        }
        Call<List<HouseDto>> call = client.getHouse(keyHouse,key);
        call.request().url().toString();
        call.enqueue(new Callback<List<HouseDto>>() {
            @Override
            public void onResponse(Call<List<HouseDto>> call, Response<List<HouseDto>> response) {
                List<MemberDto> listeMember = response.body().get(0).getMembers();
                RecyclerView myRecyclerView = findViewById(R.id.myRecyclerView);
                LinearLayoutManager layout= new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL, false);
                myRecyclerView.setLayoutManager(layout);
                myRecyclerView.setAdapter( new PrefectAdapter(listeMember));

            }

            @Override
            public void onFailure(Call<List<HouseDto>> call, Throwable t) {
                Toast.makeText(PrefectActivity.this, "Please let the hat make its opinion first!", Toast.LENGTH_SHORT).show();
            }
        });



    }}