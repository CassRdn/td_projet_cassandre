package com.example.hogwartsguide;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hogwartsguide.dto.SpellDto;

public class SpellViewHolder extends RecyclerView.ViewHolder {

    private TextView spell;
    private TextView effect;
    public SpellViewHolder(@NonNull View itemView) {
        super(itemView);
        spell = itemView.findViewById(R.id.spell);
        effect = itemView.findViewById(R.id.effect);
    }
    void display(SpellDto spellDto) {
        spell.setText(spellDto.getSpell());
        effect.setText(spellDto.getEffect());
    }
}
