package com.example.hogwartsguide.dto;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.Objects;

public class SortingHatDto {

    private String houseName;

    public SortingHatDto() {
    }

    public SortingHatDto(String houseName) {
        this.houseName = houseName;
    }

    public String getHouseName() {
        return houseName;
    }

    public void setHouseName(String houseName) {
        this.houseName = houseName;
    }

    public SortingHatDto withHouseName(String houseName) {
        this.houseName = houseName;
        return this;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SortingHatDto that = (SortingHatDto) o;
        return Objects.equals(houseName, that.houseName);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public int hashCode() {
        return Objects.hash(houseName);
    }

    @Override
    public String toString() {
        return "SortingHatDto{" +
                "houseName='" + houseName + '\'' +
                '}';
    }
}
