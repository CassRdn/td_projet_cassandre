package com.example.hogwartsguide.dto;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class SpellDto {

    private String id;
    private String spell;
    private String type;
    private String effect;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public SpellDto() {
    }

    /**
     *
     * @param spell
     * @param effect
     * @param id
     * @param type
     */
    public SpellDto(String id, String spell, String type, String effect) {
        super();
        this.id = id;
        this.spell = spell;
        this.type = type;
        this.effect = effect;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SpellDto withId(String id) {
        this.id = id;
        return this;
    }

    public String getSpell() {
        return spell;
    }

    public void setSpell(String spell) {
        this.spell = spell;
    }

    public SpellDto withSpell(String spell) {
        this.spell = spell;
        return this;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public SpellDto withType(String type) {
        this.type = type;
        return this;
    }

    public String getEffect() {
        return effect;
    }

    public void setEffect(String effect) {
        this.effect = effect;
    }

    public SpellDto withEffect(String effect) {
        this.effect = effect;
        return this;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public SpellDto withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SpellDto spellDto = (SpellDto) o;
        return id.equals(spellDto.id) &&
                Objects.equals(spell, spellDto.spell) &&
                Objects.equals(type, spellDto.type) &&
                Objects.equals(effect, spellDto.effect) &&
                Objects.equals(additionalProperties, spellDto.additionalProperties);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public int hashCode() {
        return Objects.hash(id, spell, type, effect, additionalProperties);
    }

    @Override
    public String toString() {
        return "SpellDto{" +
                "id='" + id + '\'' +
                ", spell='" + spell + '\'' +
                ", type='" + type + '\'' +
                ", effect='" + effect + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}