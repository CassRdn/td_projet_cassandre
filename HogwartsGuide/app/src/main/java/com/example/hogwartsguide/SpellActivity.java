package com.example.hogwartsguide;

import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hogwartsguide.dto.SpellDto;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SpellActivity extends AppCompatActivity {
    private String url = "https://www.potterapi.com/v1/";
    private String key = "$2a$10$at2TU1IFUGGfbh8aPHwM..lfM2IaOHNp5/NiU6NLpdKFY2tH4KxDi";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spell);
        final List<SpellDto> spells;

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();
        HogwartsClient client = retrofit.create(HogwartsClient.class);
        Call<List<SpellDto>> call = client.getSpells(key);
        call.enqueue(new Callback<List<SpellDto>>() {
            @Override
            public void onResponse(Call<List<SpellDto>> call, Response<List<SpellDto>> response) {
                List<SpellDto> list = response.body();
                RecyclerView myRecyclerView2 = findViewById(R.id.myRecyclerView2);
                LinearLayoutManager layout= new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL, false);
                myRecyclerView2.setLayoutManager(layout);
                myRecyclerView2.setAdapter( new SpellAdapter(list));

            }

            @Override
            public void onFailure(Call<List<SpellDto>> call, Throwable t) {
                Toast.makeText(SpellActivity.this, "Error...!!!", Toast.LENGTH_SHORT).show();
            }
        });
    }}