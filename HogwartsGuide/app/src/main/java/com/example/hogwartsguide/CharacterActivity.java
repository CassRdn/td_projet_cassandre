package com.example.hogwartsguide;

import android.app.AppComponentFactory;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hogwartsguide.dto.CharacterDto;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CharacterActivity extends AppCompatActivity {
    private String url = "https://www.potterapi.com/v1/";
    private String key = "$2a$10$at2TU1IFUGGfbh8aPHwM..lfM2IaOHNp5/NiU6NLpdKFY2tH4KxDi";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character);
        final List<CharacterDto> characters;

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();
        HogwartsClient client = retrofit.create(HogwartsClient.class);
        Call<List<CharacterDto>> call = client.getCharacters(key);
        call.request().url().toString();
        call.enqueue(new Callback<List<CharacterDto>>() {
            @Override
            public void onResponse(Call<List<CharacterDto>> call, Response<List<CharacterDto>> response) {
                List<CharacterDto> list = response.body();
                RecyclerView myRecyclerView = findViewById(R.id.myRecyclerView);
                LinearLayoutManager layout= new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL, false);
                            myRecyclerView.setLayoutManager(layout);
                myRecyclerView.setAdapter( new CharacterAdapter(list));

            }

            @Override
            public void onFailure(Call<List<CharacterDto>> call, Throwable t) {
                Toast.makeText(CharacterActivity.this, "Error...!!!", Toast.LENGTH_SHORT).show();
            }
        });


}}
