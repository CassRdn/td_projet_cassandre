package com.example.hogwartsguide;


import com.example.hogwartsguide.dto.CharacterDto;
import com.example.hogwartsguide.dto.HouseDto;
import com.example.hogwartsguide.dto.SpellDto;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface HogwartsClient {


    @Headers({"key:$2a$10$at2TU1IFUGGfbh8aPHwM..lfM2IaOHNp5/NiU6NLpdKFY2tH4KxDi"})
    @GET("sortingHat")
    Call<String> SortingHat();

    @GET("houses")
    Call<List<HouseDto>> getHouses(@Query("key") String key);


    @GET("houses/{houseId}")
    Call<List<HouseDto>> getHouse(@Path("houseId") String houseId, @Query("key") String key);


    @GET("characters")
    Call<List<CharacterDto>> getCharacters(@Query("key") String key);

    @GET("spells")
    Call<List<SpellDto>> getSpells(@Query("key") String key);
}
