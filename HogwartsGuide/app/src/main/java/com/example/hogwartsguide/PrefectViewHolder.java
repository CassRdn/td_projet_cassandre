package com.example.hogwartsguide;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hogwartsguide.dto.MemberDto;

public class PrefectViewHolder extends RecyclerView.ViewHolder {
    private TextView characterText;

    public PrefectViewHolder(@NonNull View itemView) {
        super(itemView);
        characterText = itemView.findViewById(R.id.characterHouse);
    }

    void display(MemberDto member) {
        characterText.setText(member.getName());
    }
}