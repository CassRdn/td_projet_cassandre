package com.example.hogwartsguide;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hogwartsguide.dto.CharacterDto;
import com.example.hogwartsguide.dto.MemberDto;

import java.util.List;

public class PrefectAdapter  extends RecyclerView.Adapter<PrefectViewHolder> {
    private List<MemberDto> characters;

    public PrefectAdapter(List<MemberDto> characters) {
        this.characters = characters;
    }

    @NonNull
    @Override
    public PrefectViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_prefect, parent, false);
        return new PrefectViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PrefectViewHolder holder, int position) {
        holder.display((characters.get(position)));
    }

    @Override
    public int getItemCount() {
        return characters.size();
    }
}